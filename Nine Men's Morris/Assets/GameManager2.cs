﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager2 : MonoBehaviour {

    private GameObject ClickedCoin;
    private GameObject ClickedPosition;

    private int selectionCheck = 0;

    private List<CoinInfo> coins = new List<CoinInfo>();

    private GameManager gm;
    private MillManager millManager;


    public float smoothing = 7f;


    

    // Use this for initialization
    void Start () {
        gm = FindObjectOfType<GameManager>();
        millManager = FindObjectOfType<MillManager>();
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetClickedCoin(GameObject g)
    {
        ClickedCoin = g;
        selectionCheck = 0;
        selectionCheck=1;
        Debug.Log(ClickedCoin.name);
    }

    public void SetClickedPosition(GameObject pos)
    {
        Debug.Log(ClickedPosition.name);
        ClickedPosition = pos;
        selectionCheck++;
        if(selectionCheck == 2)
        {
            MoveCoin();
        }
    }

    private void MoveCoin()
    {
        if (LegalMove(ClickedPosition,ClickedCoin))
        {
            Animate(ClickedCoin, ClickedPosition);

            // get PositionInfo component and set position state to Busy
            PositionInfo p = ClickedPosition.GetComponent<PositionInfo>();
            p.InfoAboutPosition = PositionInfo.PosInfo.BUSY;

            //check mill
            if (gm.turn == GameManager.Turns.PLAYER1)
            {
                millManager.CheckMillsP1();
            }
            else if(gm.turn == GameManager.Turns.PLAYER2)
            {
                millManager.CheckMillsP2();
            }

            if (gm.IsMill())
            {
                Debug.Log("Mill");
            }
            else if(gm.turn == GameManager.Turns.PLAYER1)
            {
                gm.turn = GameManager.Turns.PLAYER2;
            }
            else if(gm.turn == GameManager.Turns.PLAYER2)
            {
                gm.turn = GameManager.Turns.PLAYER1;
            }






            //change turn
            //check count

            //when coin moves check if it was in mill
            //if true delete from list
            //if false nothing        
        }
    }

    private void  Animate(GameObject g1, GameObject g2)
    {
        //while(Vector3.Distance(g1.transform.position, g2.transform.position) > 0.01f)
        //{
        //    g1.transform.position = Vector3.Lerp(g1.transform.position, g2.transform.position, smoothing * Time.deltaTime);
        //}

        g1.transform.position = g2.transform.position;


        
    }

    private void SetPosToFree(CoinInfo c)
    {
        foreach (PositionInfo p in FindObjectsOfType<PositionInfo>())
        {
            if (p.GetPosName() == c.GetPositionName())
            {
                p.InfoAboutPosition = PositionInfo.PosInfo.FREE;
                //has to be moved elsewhere
                //if(gm.turn == GameManager.Turns.PLAYER1)
                //{
                //    gm.turn = GameManager.Turns.PLAYER2;
                //}
                //else if(gm.turn == GameManager.Turns.PLAYER2)
                //{
                //    gm.turn = GameManager.Turns.PLAYER1;
                //}
            }
        }
    }

    private bool LegalMove(GameObject desiredPosition, GameObject currentPosition)
    {
        //coins.Clear();    
        //foreach(CoinInfo coin in FindObjectsOfType<CoinInfo>())
        //{
        //    coins.Add(coin);
        //}
        //CoinInfo c = coinPosition.GetComponent<CoinInfo>();

        CoinInfo c = currentPosition.GetComponent<CoinInfo>();       
        //foreach (PositionInfo p in FindObjectsOfType<PositionInfo>())
        //{
        //    if (p.GetPosName() == c.GetPositionName())
        //    {
        //        p.InfoAboutPosition = PositionInfo.PosInfo.FREE;
        //    }
        //}
        
        

        //CoinInfo desiredPos = desiredPosition.GetComponent<CoinInfo>();
        PositionInfo desiredPos = desiredPosition.GetComponent<PositionInfo>();



        if (c.GetPositionName() == "A0")
        {
            //neighbors A7,A1,B0                
            if (desiredPos.GetPosName() == "A7" || desiredPos.GetPosName() == "A1" || desiredPos.GetPosName() == "B0")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "A1")
        {
            //neighbors A0,A2
            if (desiredPos.GetPosName() == "A0" || desiredPos.GetPosName() == "A2")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "A2")
        {
            //neighbors A1,A3,B2
            if (desiredPos.GetPosName() == "A1" || desiredPos.GetPosName() == "A3" || desiredPos.GetPosName() == "B2")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "A3")
        {
            //neighbors A2,A4
            if (desiredPos.GetPosName() == "A2" || desiredPos.GetPosName() == "A4")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "A4")
        {
            //neighbors A3,A5,B4
            if (desiredPos.GetPosName() == "A3" || desiredPos.GetPosName() == "A5" || desiredPos.GetPosName() == "B4")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "A5")
        {
            //neighbors A4,A6
            if (desiredPos.GetPosName() == "A4" || desiredPos.GetPosName() == "A6")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "A6")
        {
            //neighbors A5,A7,B6
            if (desiredPos.GetPosName() == "A5" || desiredPos.GetPosName() == "A7" || desiredPos.GetPosName() == "B6")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "A7")
        {
            //neighbors A0,A6
            if (desiredPos.GetPosName() == "A0" || desiredPos.GetPosName() == "A6")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "B0")
        {
            //neighbors B1,B7,A0,C0
            if (desiredPos.GetPosName() == "B1" || desiredPos.GetPosName() == "B7" || desiredPos.GetPosName() == "A0" || desiredPos.GetPosName() == "C0")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "B1")
        {
            //neighbors B0,B2
            if (desiredPos.GetPosName() == "B0" || desiredPos.GetPosName() == "B2")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "B2")
        {
            //neighbors B1,B3,A2,C2
            if (desiredPos.GetPosName() == "B1" || desiredPos.GetPosName() == "B3" || desiredPos.GetPosName() == "A2" || desiredPos.GetPosName() == "C2")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "B3")
        {
            //neighbors B2,B4
            if (desiredPos.GetPosName() == "B2" || desiredPos.GetPosName() == "B4")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "B4")
        {
            //neighbors B3,B5,A4,C4
            if (desiredPos.GetPosName() == "B3" || desiredPos.GetPosName() == "B5" || desiredPos.GetPosName() == "A4" || desiredPos.GetPosName() == "C4")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "B5")
        {
            //neighbors B4,B6
            if (desiredPos.GetPosName() == "B4" || desiredPos.GetPosName() == "B6")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "B6")
        {
            //neighbors B5,B7,A6,C6
            if (desiredPos.GetPosName() == "B5" || desiredPos.GetPosName() == "B7" || desiredPos.GetPosName() == "A6" || desiredPos.GetPosName() == "C6")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "B7")
        {
            //neighbors B0,B6
            if (desiredPos.GetPosName() == "B0" || desiredPos.GetPosName() == "B6")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "C0")
        {
            //neighbors C7,C1,B0
            if (desiredPos.GetPosName() == "C7" || desiredPos.GetPosName() == "C1" || desiredPos.GetPosName() == "B0")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "C1")
        {
            //neighbors C0,C2
            if (desiredPos.GetPosName() == "C0" || desiredPos.GetPosName() == "C2")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "C2")
        {
            //neighbors C1,C3,B2
            if (desiredPos.GetPosName() == "C1" || desiredPos.GetPosName() == "C3" || desiredPos.GetPosName() == "B2")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "C3")
        {
            //neighbors C2,C4
            if (desiredPos.GetPosName() == "C2" || desiredPos.GetPosName() == "C4")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "C4")
        {
            //neighbors C3,C5,B4
            if (desiredPos.GetPosName() == "C3" || desiredPos.GetPosName() == "C5" || desiredPos.GetPosName() == "B4")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "C5")
        {
            //neighbors C4,C6
            if (desiredPos.GetPosName() == "C4" || desiredPos.GetPosName() == "C6")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "C6")
        {
            //neighbors C5,C7,B6
            if (desiredPos.GetPosName() == "C5" || desiredPos.GetPosName() == "C7" || desiredPos.GetPosName() == "B6")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        else if (c.GetPositionName() == "C7")
        {
            //neighbors C0,C6
            if (desiredPos.GetPosName() == "C0" || desiredPos.GetPosName() == "C6")
            {
                if (desiredPos.InfoAboutPosition == PositionInfo.PosInfo.FREE)
                {
                    desiredPos.InfoAboutPosition = PositionInfo.PosInfo.BUSY;
                    SetPosToFree(c);
                    return true;
                }
            }
        }
        
        




            return false;
        
    }


}
