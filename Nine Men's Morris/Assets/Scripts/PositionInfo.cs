﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionInfo : MonoBehaviour {

    private GameManager gm;
    private GameManager2 gm2;
    

    public enum PosInfo
    {
        FREE,
        BUSY,
    }

    public PosInfo InfoAboutPosition;


	// Use this for initialization
	void Start () {
        gm = FindObjectOfType<GameManager>();
        gm2 = FindObjectOfType<GameManager2>();
	}

    // Update is called once per frame
    void Update() {
        //if (gm.returnPhase2())
        //{
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        if (gm.turn == GameManager.Turns.PLAYER1 && !gm.IsMill())
        //        {
        //            if (gameObject.tag == "P1COIN")
        //            {
        //                gm2.SetClickedPosition(gameObject);
        //            }
        //        }
        //        else if (gm.turn == GameManager.Turns.PLAYER2 && !gm.IsMill())
        //        {
        //            if (gameObject.tag == "P2COIN")
        //            {
        //                gm2.SetClickedPosition(gameObject);
        //            }
        //        }
        //    }
        //}
	}

    public string GetPosName()
    {
        return gameObject.name;
    }

    public PosInfo getPosInfo()
    {
        return InfoAboutPosition;
    }


    private void OnMouseUp()
    {
        if (gm.returnPhase2())
        {
            if (gm.turn == GameManager.Turns.PLAYER1 && !gm.IsMill())
            {
                if (gameObject.tag == "P1COIN")
                {                  
                    gm2.SetClickedPosition(gameObject);
                    Debug.Log("done P1");
                }
            }
            else if (gm.turn == GameManager.Turns.PLAYER2 && !gm.IsMill())
            {
                if (gameObject.tag == "P2COIN")
                {
                    gm2.SetClickedPosition(gameObject);
                    Debug.Log("done P2");
                }
            }
        }
    }
    private void OnMouseDown()
    {
        if (gm.returnPhase1())
        {
            if (!gm.IsMill())
            {
                if (InfoAboutPosition == PosInfo.BUSY)
                {
                    Debug.Log("nemože na poziciju");
                }
                else if (gm.returnPhase1())
                {
                    gm.MovePieceToBoard(gameObject, gameObject.name);
                    if (gm.returnPhase1())
                    {
                        InfoAboutPosition = PosInfo.BUSY;
                    }
                    //Debug.Log(gameObject.name);
                    //Debug.Log(InfoAboutPosition);
                }
            }
        }

        if (gm.returnPhase2())
        {
            if (gm.turn == GameManager.Turns.PLAYER1 && !gm.IsMill())
            {
                if (gameObject.tag == "P1COIN")
                {
                    gm2.SetClickedPosition(gameObject);
                }
            }
            else if (gm.turn == GameManager.Turns.PLAYER2 && !gm.IsMill())
            {
                if (gameObject.tag == "P2COIN")
                {
                    gm2.SetClickedPosition(gameObject);
                }
            }
        }


        
         
    }


   
}


