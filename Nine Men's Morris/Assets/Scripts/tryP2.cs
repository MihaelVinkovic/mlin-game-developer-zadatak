﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tryP2 : MonoBehaviour {
    private GameManager gm;
    private GameManager2 gm2;


    // Use this for initialization
    void Start () {
        gm = FindObjectOfType<GameManager>();
        gm2 = FindObjectOfType<GameManager2>();
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    private void OnMouseDown()
    {
        
            if (gm.returnPhase2())
            {
                
                    if (gm.turn == GameManager.Turns.PLAYER1 && !gm.IsMill())
                    {
                        if (gameObject.tag == "P1COIN")
                        {
                            gm2.SetClickedPosition(gameObject);
                        }
                    }
                    else if (gm.turn == GameManager.Turns.PLAYER2 && !gm.IsMill())
                    {
                        if (gameObject.tag == "P2COIN")
                        {
                            gm2.SetClickedPosition(gameObject);
                        }
                    }
                }
            
        }


}
