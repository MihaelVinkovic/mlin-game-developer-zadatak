﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MillManager : MonoBehaviour {


    public List<CoinInfo> P1Coins = new List<CoinInfo>();
    public List<CoinInfo> P2Coins = new List<CoinInfo>();
    public List<PositionInfo> PosInfo = new List<PositionInfo>();

    private List<string> oldMils = new List<string>();    

    private CoinInfo coinInfo;
    private GameManager gameManager;

    private string millPos;
    bool oldM = false;
    


    // Use this for initialization
    void Start () {

        coinInfo = FindObjectOfType<CoinInfo>();
        foreach(GameObject pos in GameObject.FindGameObjectsWithTag("Pos"))
        {
            PosInfo.Add(pos.GetComponent<PositionInfo>());
        }


        //debuging only
        //CheckMillsP1();
        //CheckMillsP2();       
        gameManager = FindObjectOfType<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
       
	}

    

    public void CheckMillsP1()
    {
        P1Coins.Clear();
        foreach(GameObject g in GameObject.FindGameObjectsWithTag("P1COIN"))
        {
            P1Coins.Add(g.GetComponent<CoinInfo>());
            //Debug.Log(g.gameObject.name);            
        }

        CheckMills(P1Coins);
    }


    public void CheckMillsP2()
    {
        P2Coins.Clear();
        foreach(GameObject g2 in GameObject.FindGameObjectsWithTag("P2COIN"))
        {
            P2Coins.Add(g2.GetComponent<CoinInfo>());
            //Debug.Log(g2.gameObject.name);            
        }
        CheckMills(P2Coins);
    }

    private void CheckMills(List<CoinInfo> l)
    {
        string first = "", second = "",  third= "";
        oldM = false;

        foreach (CoinInfo c in l)
        {
            first = c.GetPositionName();
            foreach(CoinInfo cb in l)
            {
                if (first == "A0")
                {/*
                    if (cb.GetPositionName() == "B0")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "C0")
                            {
                                third = cbd.GetPositionName();
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                    else*/ if (cb.GetPositionName() == "A7")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "A1")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                ////Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "A2")
                {
                    /*if (cb.GetPositionName() == "B2")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "C2")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }

                    }
                    else */if (cb.GetPositionName() == "A3")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "A1")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "A4")
                {
                    if (cb.GetPositionName() == "B4")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "C4")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                    else if (cb.GetPositionName() == "A3")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "A5")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "A6")
                {
                    /*if (cb.GetPositionName() == "B6")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "C6")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                    else */if (cb.GetPositionName() == "A5")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "A7")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "B0")
                {
                    if (cb.GetPositionName() == "C0")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "A0")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL AT B0 C0 A0");
                            }


                        }
                    }
                    else if (cb.GetPositionName() == "B7")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "B1")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "B2")
                {
                    if (cb.GetPositionName() == "A2")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "C2")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                    else if (cb.GetPositionName() == "B1")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "B3")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "B4")
                {
                    /*if (cb.GetPositionName() == "C4")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "A4")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                    else */if (cb.GetPositionName() == "B3")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "B5")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "B6")
                {
                    if (cb.GetPositionName() == "A6")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "C6")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                    else if (cb.GetPositionName() == "B5")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "B7")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "C0")
                {
                    if (cb.GetPositionName() == "C7")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "C1")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "C2")
                {
                    if (cb.GetPositionName() == "C1")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "C3")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "C4")
                {
                    if (cb.GetPositionName() == "C5")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "C3")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }
                else if (first == "C6")
                {
                    if (cb.GetPositionName() == "C5")
                    {
                        second = cb.GetPositionName();
                        foreach (CoinInfo cbd in l)
                        {
                            if (cbd.GetPositionName() == "C7")
                            {
                                third = cbd.GetPositionName();
                                //mill happened
                                millPos = first + second + third;
                                AddMillToList();
                                //Debug.Log("MILL");
                            }
                        }
                    }
                }

            }
        }
        //Debug.Log("NO MILL");
    }

    private void AddMillToList()
    {
        

        foreach (string s in oldMils)
        {
            if (s == millPos)
            {
                oldM = true;
            }
           
        }
        if (!oldM)
        {
            
            oldMils.Add(millPos);
            //mill happened
            SetCoinStateTOMill(millPos);
            gameManager.setMill(true);
            Debug.Log("Mill");
            Debug.Log("** Print list content  **");
            Debug.Log("List length: " + oldMils.Count);
            string listContent="";
            foreach(string s in oldMils)
            {
                listContent += s+", ";
            }
            Debug.Log("List content: "  + listContent);
            Debug.Log("*** End of list ***");
        }
        else
        {
            oldM = false;
        }
        
    }
    
    private void SetCoinStateTOMill(string name)
    {
        string No1 = name.Substring(0, 2);
        string No2 = name.Substring(2, 2);
        string No3 = name.Substring(4, 2);
        //Debug.Log("Print: " + No1 + " " + No2 + " " + No3); 
        foreach(CoinInfo c in P1Coins)
        {
            if (c.GetPositionName() == No1)
            {
                c.SetCoinState("MILL");
            }
            else if (c.GetPositionName() == No2)
            {
                c.SetCoinState("MILL");
            }
            else if (c.GetPositionName() == No3)
            {
                c.SetCoinState("MILL");
            }
        }

        foreach (CoinInfo c in P2Coins)
        {
            if (c.GetPositionName() == No1)
            {
                c.SetCoinState("MILL");
            }
            else if (c.GetPositionName() == No2)
            {
                c.SetCoinState("MILL");
            }
            else if (c.GetPositionName() == No3)
            {
                c.SetCoinState("MILL");
            }
        }
    }





}
