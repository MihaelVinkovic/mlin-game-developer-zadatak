﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public GameObject[] player1;    // pieces owned by Player 1
    public GameObject[] player2;    // pieces owned by Player 2

    public enum Turns { PLAYER1, PLAYER2 };
    public Turns turn;

    public float LerpSpeed = 1f;

    private bool phase1 = true;
    private bool phase2 = false;
    private bool mill = false;

    // 0 initial value ->pass
    // 1 value when mill happens and player must destroy other player's piece
    // 2 value when player successfully destroyed other player's piece
    private int pieceDestroyed = 0;
    
    private int numberOfPlaysByP1=0;
    private int numberOfPlaysByP2=0;

    private Text turnText;
    private Text piecesToSetTextP1;
    private Text piecesToSetTextP2;

    private CoinInfo coinInfo;
    private MillManager millManager;


    

    public void MovePieceToBoard(GameObject g, string name)
    {        
        if ((9 - numberOfPlaysByP2) != 0)
        {
            if (pieceDestroyed == 0 || pieceDestroyed == 2)
            {
                if (turn == Turns.PLAYER1)
                {
                    Vector3 v = new Vector3(0, 0, -1);
                    v += g.transform.position;
                    player1[numberOfPlaysByP1].transform.position = Vector3.Lerp(transform.position, v, LerpSpeed);
                    coinInfo = player1[numberOfPlaysByP1].GetComponent<CoinInfo>();
                    numberOfPlaysByP1++;

                    //set coin state to normal and set coin master to p1

                    coinInfo.SetCoinMaster("P1");
                    coinInfo.SetCoinState("NORMAL");
                    coinInfo.setPositionName(name);
                    //Debug.Log("This is position name: " + name);
                    millManager.CheckMillsP1();

                    // check if there is mill, if true , take opponent's piece, else pass turn to player2
                    if (IsMill())
                    {
                        //enable power of destruction for player 1, so he can destroy opponent's piece
                        //wait for P1 to click on enemy piece
                        //check if coin state != mill
                        //check if coin master == P2 so P1 can't destroy his pieces
                        PieceDestroyed(1);


                    }
                    else
                    {


                        turn = Turns.PLAYER2;
                        setMill(false);
                    }



                }
                else if (turn == Turns.PLAYER2)
                {
                    Vector3 v = new Vector3(0, 0, -1);
                    v += g.transform.position;
                    player2[numberOfPlaysByP2].transform.position = Vector3.Lerp(transform.position, v, LerpSpeed);
                    coinInfo = player2[numberOfPlaysByP2].GetComponent<CoinInfo>();
                    numberOfPlaysByP2++;

                    //set coin state to normal and set coin master to p2

                    coinInfo.SetCoinMaster("P2");
                    coinInfo.SetCoinState("NORMAL");
                    coinInfo.setPositionName(name);


                    millManager.CheckMillsP2();

                    if (IsMill())
                    {
                        PieceDestroyed(1);
                    }
                    else
                    {


                        turn = Turns.PLAYER1;
                        setMill(false);
                    }


                }
            }
        }else
        {
            phase1 = false;
            phase2 = true;
            Debug.Log("Phase 1 successfully finished !");
        }
        
    }


   

    // Use this for initialization
    void Start () {
        turnText = GameObject.Find("MessageDialog").GetComponent<Text>();
        piecesToSetTextP1 = GameObject.Find("P1_PiecesToSet").GetComponent<Text>();
        piecesToSetTextP2 = GameObject.Find("P2_PiecesToSet").GetComponent<Text>();
        millManager = GameObject.FindObjectOfType<MillManager>();
    }

    // Update is called once per frames
    void Update()
    {
        if((9-numberOfPlaysByP2) == 0)
        {
            phase1 = false;
            phase2 = true;
        }
        if (turn == Turns.PLAYER1)
        {
            turnText.text = "Player 1 \nplace piece";
            if (mill)
            {
                turnText.text = "Player 1 remove \n red piece";
            }

            if (phase2)
            {
                turnText.text = "Player 1 \nmove piece";
            }
        }
        else if (turn == Turns.PLAYER2)
        {
            turnText.text = "Player 2 \nplace piece";
            if (mill)
            {
                turnText.text = "Player 2 remove \n blue piece";
            }
            if (phase2)
            {
                turnText.text = "Player 2 \nmove piece";
            }
        }

        piecesToSetTextP1.text = "Pieces to set: " + (9 - numberOfPlaysByP1);
        piecesToSetTextP2.text = "Pieces to set: " + (9 - numberOfPlaysByP2);


       
       


        

        //if (Input.GetMouseButtonDown(0))
        //{

        //    RaycastHit2D hit = Physics2D.Raycast(cam.transform.position, Input.mousePosition);
          
        //    if (hit.transform.gameObject.tag == "Pos")
        //    {

        //        Debug.Log(hit.transform.gameObject.name);                
        //    }
        //    else
        //    {
        //        Debug.Log("ne");
        //    }
        //}
    } 


    public bool returnPhase1()
    {
        if (phase1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public bool returnPhase2()
    {
        if (phase2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void setMill(bool mil)
    {
        mill = mil;
    }

    public bool IsMill()
    {
        return mill;
    }


    public void PieceDestroyed(int value)
    {
        pieceDestroyed = value;
    }


    public int getPieceDestroyed()
    {
        return pieceDestroyed;
    }
   
}

