﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinInfo : MonoBehaviour {

    GameManager2 gm2;


    public enum CoinState
    {
        MILL,
        NORMAL,
        JUMP
    }
    public CoinState coinState;


    public enum CoinMaster
    {
        P1,
        P2
    }

    public CoinMaster coinMaster;

    public string PositionName ="NOTPLACED";

    private GameManager gm;
    

	// Use this for initialization
	void Start () {
        gm = FindObjectOfType<GameManager>();
        gm2 = FindObjectOfType<GameManager2>();
        //coinState = CoinState.NORMAL;
        //coinMaster = CoinMaster.PLAYER1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public string GetPositionName()
    {
        return PositionName;
    }

    public void setPositionName(string name)
    {
        PositionName = name;
    }


    public void SetCoinMaster(string player)
    {
        if(player == "P1")
        {
            coinMaster = CoinMaster.P1;
        }
        else if(player == "P2")
        {
            coinMaster = CoinMaster.P2;
        }
    }
   

    public void SetCoinState(string state)
    {
        if(state == "NORMAL")
        {
            coinState = CoinState.NORMAL;
        }
        else if(state == "MILL")
        {
            coinState = CoinState.MILL;
        }
        else if(state == "JUMP")
        {
            coinState = CoinState.JUMP;
        }
    }

    private void OnMouseDown()
    {
        //Debug.Log("---");
        //Debug.Log("coininfo");
        //Debug.Log(gm.IsMill());
        //Debug.Log(gm.turn);
        //Debug.Log(gameObject.tag);
        //Debug.Log("---");
        if (gm.IsMill() && gm.turn == GameManager.Turns.PLAYER1)
        {
            if(gameObject.tag == "P2COIN")
            {
                if(coinState == CoinState.NORMAL)
                {
                    string pos = GetPositionName();
                    foreach(PositionInfo p in GameObject.FindObjectsOfType<PositionInfo>())
                    {
                        if(p.name == pos)
                        {
                            p.InfoAboutPosition = PositionInfo.PosInfo.FREE;
                        }
                    }
                    Destroy(gameObject);
                    gm.setMill(false);
                    gm.PieceDestroyed(2);
                    gm.turn = GameManager.Turns.PLAYER2;
                    

                }
            }
        }else if(gm.IsMill() && gm.turn == GameManager.Turns.PLAYER2)
        {
            if(gameObject.tag == "P1COIN")
            {
                if(coinState == CoinState.NORMAL)
                {
                    string pos = GetPositionName();
                    foreach (PositionInfo po in GameObject.FindObjectsOfType<PositionInfo>())
                    {
                        if (po.name == pos)
                        {
                            po.InfoAboutPosition = PositionInfo.PosInfo.FREE;
                        }
                    }
                    Destroy(gameObject);
                    gm.setMill(false);
                    gm.turn = GameManager.Turns.PLAYER1;
                    gm.PieceDestroyed(2);
                }
            }
        }

        if (gm.returnPhase2())
        {
            if (gm.turn == GameManager.Turns.PLAYER1 && !gm.IsMill())
            {
                if (gameObject.tag == "P1COIN")
                {
                    gm2.SetClickedCoin(gameObject);
                }
            }
            else if (gm.turn == GameManager.Turns.PLAYER2 && !gm.IsMill())
            {
                if (gameObject.tag == "P2COIN")
                {
                    gm2.SetClickedCoin(gameObject);
                }
            }


        }
    }

}
